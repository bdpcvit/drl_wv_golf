/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
 */
#include "timers.h"
#include "sysutils.h"



void Systick_init(void)
{
    TIM4_PSCR = PSC_MS;    /* Set prescaler to 3 TIM_CLK = F_PCS /(2^TIM4_PSCR)
                              for CPU_CLK 2000000 TIM4_PSCR=3
                              for CPU_CLK 4000000 TIM4_PSCR=4
                              for CPU_CLK 8000000 TIM4_PSCR=5
                              for CPU_CLK 16000000 TIM4_PSCR=6 */
    TIM4_ARR = 250;         // Set ARR to 250
    TIM4_CNTR = 0;
    TIM4_IER_UIE = 1;       //Enable interrupt
    TIM4_CR1_CEN = 1;       // Start timer
}


void Timer_Set(Timer_t *t, time_t interval)
{
    t->interval = interval;
    t->start = Get_Time();
    t->flag = started;
}

void Timer_Reset(Timer_t *t)
{
    t->start += t->interval;
    t->flag = started;
}

void Timer_Restart(Timer_t *t)
{
    t->start = Get_Time();
    t->flag = started;
}

Timer_flag Timer_Expired(Timer_t *t)
{
    if (t->flag)
        return t->flag;
    if ((time_t) (Get_Time() - t->start) >= (time_t) t->interval)
    {
        t->flag = stopped;
        return first;
    }
    return started;
}

inline static time_t Get_Time(void)
{
    return sys_tick;
}

/* [] END OF FILE */
