#ifndef _ADC
#define _ADC

#include "sysutils.h"
#include "delay.h"

#if ADC_E == 0
#error Please enable ADC clock in sysutil header file
#endif

//Static local wariables


// function reference
uint16_t ADC_GetData(uint8_t chanel);
void ADC_Start(void);


//Function
void ADC_Start(void)
{
    ADC_CR1_SPSEL = 0x02;   //Fadc = sys_clk/4
    ADC_CR2_ALIGN = 1;
}


uint16_t ADC_GetData(uint8_t chanel)
{
    uint16_t value = 0;

    ADC_CSR_CH = chanel;      // Select Chanel
    ADC_TDRL = chanel;        // Disable Shmitd triger on chanel
    ADC_CR3_DBUF = 1;
    ADC_CR1_CONT = 1;
    ADC_CR1_ADON = 1;         // Start conversion
    ADC_CR1_ADON = 1;
    while (ADC_CSR_EOC == 0);
    ADC_CR1_ADON = 0;
    ADC_CSR_EOC = 0;

    value = ADC_DB0RL;
    value += (ADC_DB0RH << 8);
    value += ADC_DB1RL;
    value += (ADC_DB1RH << 8);
    value += ADC_DB2RL;
    value += (ADC_DB2RH << 8);
    value += ADC_DB3RL;
    value += (ADC_DB3RH << 8);
    value += ADC_DB4RL;
    value += (ADC_DB4RH << 8);
    value += ADC_DB5RL;
    value += (ADC_DB5RH << 8);
    value += ADC_DB6RL;
    value += (ADC_DB6RH << 8);
    value += ADC_DB7RL;
    value += (ADC_DB7RH << 8);
    value += ADC_DB8RL;
    value += (ADC_DB8RH << 8);
    value += ADC_DB9RL;
    value += (ADC_DB9RH << 8);
    return value/10;
}

#endif