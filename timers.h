/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef TIMERS_H
#define TIMERS_H

#include <stdint.h>
#include "sysutils.h"

#if TIM4_6_E == 0
#error Please enable TIM4_6 clock in sysutil header file
#endif

#ifndef CPU_CLK
#error Define CPU_CLK
#endif

#if CPU_CLK == 2
#define PSC_MS 3
#elif CPU_CLK == 4
#define PSC_MS 4
#elif CPU_CLK == 8
#define PSC_MS 5
#elif CPU_CLK == 16
#define PSC_MS 6
#endif

void Systick_init(void);

typedef uint16_t time_t;

#define SEC     (time_t)1000
#define MINUTE	SEC * 60u
#define HOUR	MINUTE * 60u

typedef enum {
    started = 0,
    stopped,
    first
} Timer_flag;

typedef struct {
    time_t start;
    time_t interval;
    Timer_flag flag;
} Timer_t;


void Timer_Set(Timer_t *t, time_t interval);
void Timer_Reset(Timer_t *t);
void Timer_Restart(Timer_t *t);
Timer_flag Timer_Expired(Timer_t *t);

inline static time_t Get_Time(void);

#endif
/* [] END OF FILE */
