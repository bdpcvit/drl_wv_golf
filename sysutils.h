#ifndef _SYSUTILS
#define _SYSUTILS

#include <iostm8s003f3.h>
#include <stdint.h>

#define CPU_CLK 2
#define DRL_DELAY   15000


#define set_far_light(x) {TIM1_CCR1H = (x)/256; TIM1_CCR1L = (x)%256;}
#define set_near_light(x) {TIM1_CCR2H = (x)/256; TIM1_CCR2L = (x)%256;}
#define set_pwm_relay(x) {TIM2_CCR2H = (x)/256; TIM2_CCR2L = (x)%256;}
// Set MS byte to compare register chanel 1
// shadow register bloked
// Set LS byte to compare register chanel 1

#define enableInterrupts() {asm("rim\n");} /* enable interrupts */

#define POWER_RELAY_MAX_PWM     80U


//Disable CLK for unused peripherials
#define TIM1_E 1
#define TIM3_E 0
#define TIM2_5_E 1
#define TIM4_6_E 1
#define UART1_2_3_E 0
#define SPI_E 0
#define I2C_E 0
#define CAN_E 0
#define ADC_E 0
#define AWU_E 0


typedef union
{
    uint8_t byte;
    struct
    {
        uint8_t f_1:1;
        uint8_t f_2:1;
        uint8_t f_3:1;
        uint8_t f_4:1;
        uint8_t f_5:1;
        uint8_t f_6:1;
        uint8_t f_7:1;
        uint8_t f_8:1;
    };
}flags_t;


#define FL_NEAR_LIGHT_EV        flags.f_1   //ON Near light
#define FL_FAR_LIGHT_EV         flags.f_2   //On FAR light
#define FL_DRL_MODE             flags.f_3   //Set if controller in DRL mode
#define FL_SYS_STATE_CHANGE     flags.f_4   //System state change.
#define FL_POWER_RELAY_STATE    flags.f_5   //Indicate state of Power relay

typedef enum
{
    IDLE = (uint8_t)0,
    LIGHT_ON,
    DELAY,
    DRL,
}light_state_t;


typedef union
{
    uint8_t byte;
    struct
    {
        uint8_t far_light:1;
        uint8_t near_light:1;
        uint8_t parking_light:1;
        uint8_t generator_state:1;
    };
}input_state_t;

#define GET_NEAR_LIGHT_STATE()      PD_IDR_IDR4
#define GET_FAR_LIGHT_STATE()       PD_IDR_IDR5
#define GET_PARKING_LIGHT_STATE()   PD_IDR_IDR6
#define GET_GENERATOR_STATE()       PA_IDR_IDR1
#define GET_DRL_SWITCH_STATE()      PD_IDR_IDR3

#define SET_POWER_LED(x)            (PB_ODR_ODR5 = x)



/*
#pragma vector=TIM4_OVR_UIF_vector
__interrupt __root void TIM4_OVR(void)
*/


// Static local variables

// Global variables
uint16_t sys_tick;
flags_t flags;


// Functions prototypes
void Sysinit(void);
uint16_t map(uint16_t var, uint16_t fromLow, uint16_t fromHigh,\
    uint16_t toLow, uint16_t toHigh);

#endif
