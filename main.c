#include <iostm8s003f3.h>
#include <stdint.h>
#include "sysutils.h"
#include "timers.h"



// ������ ������� ������ ����������
//const uint8_t modes[] = {
//    0x00,  // 0b00000000  ��������� ��������
//    0xFF,  // 0b11111111  ����� ���������
//    0x0F,  // 0b00001111  ������� �� 0.5 ���
//    0x01,  // 0b00000001  �������� ������� ��� � �������
//    0x05,  // 0b00000101  ��� �������� ������� ��� � �������
//    0x15,  // 0b00010101  ��� �������� ������� ��� � �������
//    0x55   // 0b01010101  ������ �������� ������� (4 ���� � �������)
//};


enum
{
    LED_OFF                 = 0x00,
    LED_DRL                 = 0xFF,
    LED_DRL_STARTING_PAUSE  = 0x0F,
    LED_GENERAL_LIGHT       = 0x01,
    LED_NO_GENERATOR_SIGNAL = 0x55
}led_mode;


#define TIMER_DELAY 2   // 2 ms delay betwen change PWM


//Function prototypes
void Near_light_SM(void);
void Far_light_SM(void);
void Power_LED_SM_Init(void);
void Power_LED_SM(void);
void Sysytem_SM(void);
void Get_system_state(void);
void Power_Relay_SM(void);
__interrupt void Systick_ISR(void);

// Global variables
const uint16_t on_var[] = {0,1,2,3,4,5,6,7,8,9,13,16,22,27,39,54,69,92,115,137,161,182,202,225,247,256};
uint16_t sys_tick;

Timer_t tm_far_timer, tm_near_timer;
Timer_t tm_led_timer;

flags_t flags;
uint8_t drl_val = 0;
//uint8_t led_mode = 0;
input_state_t input_state;

void main(void)
{
    Sysinit();
    Power_LED_SM_Init();
    flags.byte = 0;
    input_state.byte = 0;

    while(1)
    {
        Get_system_state();
        Sysytem_SM();
        Near_light_SM();
        Far_light_SM();
        Power_LED_SM();
        Power_Relay_SM();
        IWDG_KR = 0xAA;     //reset IWDG
    }
}


void Sysytem_SM(void)
{
    static input_state_t input_state_bkp;

    if(input_state.byte == input_state_bkp.byte)
    {
        FL_SYS_STATE_CHANGE = 0;
        return;
    }

    input_state_bkp.byte = input_state.byte;
    FL_SYS_STATE_CHANGE = 1;

    /* input_state.byte
     * xxxx 0000
     *      |||| - Generator  (0x08)
     *       ||| - Park light (0x04)
     *        || - Near light (0x02)
     *         | - Far light  (0x01)
    */
    switch (input_state.byte & 0x03)    //Select only last 2 bits
    {
        case 0x00:
            if(input_state.generator_state && (input_state.parking_light == 0) )
            {
                FL_DRL_MODE = 1;
                FL_FAR_LIGHT_EV = 1;
                led_mode = LED_DRL;
            }else
            {
                FL_DRL_MODE = 0;
                set_far_light(0);    //OFF Far light
                set_near_light(0);   //OFF Near light
                set_pwm_relay(0);    //OFF output power relay
            }
        break;

        case 0x01:
            FL_FAR_LIGHT_EV = 1;
            FL_DRL_MODE = 0;
        break;

        case 0x02:
            FL_NEAR_LIGHT_EV = 1;
            FL_DRL_MODE = 0;
        break;

        default:
            FL_DRL_MODE = 0;
            set_far_light(0);    //OFF Far light
            set_near_light(0);   //OFF Near light
            set_pwm_relay(0);    //OFF output power relay
        break;
    }
}


void Power_Relay_SM(void)
{
    static uint8_t pr_state = 0;
    static Timer_t tm_pr_timer;


    if(input_state.byte && FL_POWER_RELAY_STATE == 0)
    {
        switch (pr_state)
        {
            case 0:
                set_pwm_relay(POWER_RELAY_MAX_PWM);
                Timer_Set(&tm_pr_timer, 30);    //��� ��������� ���� (������� ��� ������)
                pr_state = 1;
            break;

            case 1:
                if(Timer_Expired(&tm_pr_timer))
                {
                    set_pwm_relay(POWER_RELAY_MAX_PWM / 3); //������� �� ����� ������� �� ����
                    FL_POWER_RELAY_STATE = 1;
                }
            break;

            default:
            break;
        }

    }
}



void Get_system_state(void)
{
    input_state.far_light  = GET_FAR_LIGHT_STATE();
    input_state.near_light = GET_NEAR_LIGHT_STATE();
    input_state.parking_light = GET_PARKING_LIGHT_STATE();
    input_state.generator_state = GET_GENERATOR_STATE();
}




//Near light
void Near_light_SM(void)
{
    static light_state_t state = IDLE;
    static uint8_t pwm = 0;

    if( (FL_NEAR_LIGHT_EV == 0) && (state == IDLE) ) return;


    switch(state)
    {
        case IDLE:
            if ( FL_NEAR_LIGHT_EV )
            {
                FL_NEAR_LIGHT_EV = 0;
                state = LIGHT_ON;
                pwm = 0;
                set_near_light(on_var[0]);
            }
        break;

        case LIGHT_ON:
            if(FL_POWER_RELAY_STATE)
            {
                set_far_light(on_var[pwm]);

                if( ++pwm > 25 )
                {
                    state = IDLE;
                }else
                {
                    state = DELAY;
                    Timer_Set(&tm_near_timer, 2);
                }
            }
        break;

        case DELAY:
            if(Timer_Expired(&tm_near_timer))
                state = LIGHT_ON;
        break;

        default:
        break;
    }
}



//Far light
void Far_light_SM(void)
{
    static light_state_t state = IDLE;
    static uint8_t pwm = 0;

    if( (FL_FAR_LIGHT_EV == 0) && (state == IDLE) ) return;


    switch(state)
    {
        case IDLE:
            if ( FL_FAR_LIGHT_EV )
            {
                FL_FAR_LIGHT_EV = 0;
                pwm = 0;
                set_far_light(on_var[0]);

                if(FL_DRL_MODE)
                {
                    Timer_Set(&tm_far_timer, DRL_DELAY);
                    state = DELAY;
                }else  state = LIGHT_ON;
            }
        break;

        case LIGHT_ON:
            if(FL_POWER_RELAY_STATE)
            {
                set_far_light(on_var[pwm]);

                pwm++;
                if( ( pwm > 25 ) || ( FL_DRL_MODE && (on_var[pwm] > drl_val) ) )
                {
                    state = IDLE;
                }else
                {
                    state = DELAY;
                    Timer_Set(&tm_far_timer, 2);
                }
            }
        break;

        case DELAY:
            if(Timer_Expired(&tm_far_timer))
                state = LIGHT_ON;
        break;

        default:
        break;
    }
}



void Power_LED_SM_Init()
{
    Timer_Set(&tm_led_timer, 125);  //125 ms timeout
}


void Power_LED_SM(void)
{
    static uint8_t blink_loop = 0;

    if(Timer_Expired(&tm_led_timer))
    {
        // ����� ���������� ���� �� ������� �����
        if(  led_mode & 1<<(blink_loop&0x07) ) SET_POWER_LED(1);
        else  SET_POWER_LED(0);
        blink_loop++;
        Timer_Restart(&tm_led_timer);
    }
}



#pragma vector = TIM4_OVR_UIF_vector  /* Symbol from I/O */
__interrupt void Systick_ISR(void)
{
    TIM4_SR_UIF = 0;    //Clear ISR flag
    sys_tick++;         //That all Folks
}
