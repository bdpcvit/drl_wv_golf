TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    timers.c \
    sysutils.c

INCLUDEPATH += "C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.3\stm8\inc"
INCLUDEPATH += "C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.3\stm8\inc\c"


DEFINES += __IAR_SYSTEMS_ICC__

HEADERS += \
    ADC.h \
    I2C.h \
    LCD.h \
    sysutils.h \
    TEA5767.h \
    timers.h
