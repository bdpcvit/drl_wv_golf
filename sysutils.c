#include "sysutils.h"
#include "timers.h"

#define HSI_OUT_CLK_2  (uint8_t)3
#define HSI_OUT_CLK_4  (uint8_t)2
#define HSI_OUT_CLK_8  (uint8_t)1
#define HSI_OUT_CLK_16  (uint8_t)0



// Function block
void Sysinit(void)
{
    CLK_PCKENR1 = TIM1_E << 7 | TIM3_E << 6 | TIM2_5_E << 5 | TIM4_6_E << 4 | \
        UART1_2_3_E << 3 | UART1_2_3_E << 2 | SPI_E << 1 | I2C_E;
    CLK_PCKENR2 = CAN_E << 7 | ADC_E << 3 | AWU_E << 2;

//  Config master clock
#if CPU_CLK == 2
#define HSI_DIVIDER HSI_OUT_CLK_2
#elif CPU_CLK == 4
#define HSI_DIVIDER HSI_OUT_CLK_4
#elif CPU_CLK == 8
#define HSI_DIVIDER HSI_OUT_CLK_8
#elif  CPU_CLK == 16
#define HSI_DIVIDER HSI_OUT_CLK_16
#endif

    CLK_CKDIVR_HSIDIV = HSI_DIVIDER;

    //GPIO config
    PA_DDR_DDR1 = 0;    //GENERATOR_STATE
    PD_DDR_DDR3 = 0;    //DRL_SWITCH_STATE
    PD_DDR_DDR4 = 0;    //NEAR_LIGHT_STATE
    PD_DDR_DDR5 = 0;    //FAR_LIGHT_STATE
    PD_DDR_DDR6 = 0;    //PARKING_LIGHT_STATE

    PB_DDR_DDR5 = 1;    //Power LED

    //TIM2 config
    TIM2_PSCR_PSC = 0;  //Fcntr = Fmaster/(2^0)
    TIM2_CR1_ARPE = 0;
    TIM2_ARRH = 0;
    TIM2_ARRL = 80U;

    //Config CCR2 chanel
    TIM2_CCMR2_OC2M = 0x06; //PWM mode 1
    set_pwm_relay(0);       //Load to TIM2_CCR2 value "0"
    TIM2_CCER1_CC2E = 1;    //Enable 2 compare module
    TIM2_CR1_CEN = 1;       //Start TIM2


    //TIM1 config
    //Select the counter clock source
    TIM1_PSCRH = 0;
    TIM1_PSCRL = 3;      //Fcntr = Fmaster/(3+1)
    // Write the desired data in the TIM1_ARR and TIM1_CCRi registers.
    TIM1_CR1_ARPE = 1;   //Update ARR after UEV
    TIM1_ARRH = 0;
    TIM1_ARRL = 255;

    //Config CCR1 chanel
    TIM1_CCMR1_OC1M = 0x06;  //PWM mode 1
    set_far_light(0);        //Load to TIM1_CCR1 value "0"
    TIM1_CCER1_CC1E = 1;     //Enable 1 compare module

    //Config CCR2 chanel
    TIM1_CCMR2_OC2M = 0x06;  //PWM mode 1
    set_near_light(0);       //Load to TIM1_CCR2 value "0"
    TIM1_CCER1_CC2E = 1;     //Enable 1 compare module
    TIM1_BKR_MOE = 1;

    TIM1_CR1_CEN = 1;   //Start TIM1
    TIM1_EGR_UG = 1;    //generate UEV to load all values to working registers


    //config IWDG
    IWDG_KR = 0xCC;     //Start IWDG
    IWDG_KR = 0x55;     //unlock config registers
    IWDG_PR = 3;        //set prescaler divider to 32
    IWDG_RLR = 219;     // Delay at 110ms
    IWDG_KR = 0xAA;     //Refresh IWDG config

    Systick_init();

    enableInterrupts();
}


uint16_t map(uint16_t var, uint16_t inMin, uint16_t inMax,\
    uint16_t outMin, uint16_t outMax)
{
    // Line equation is y(x)=kx+b
    if(var < inMin) var = inMin;
    else if(var > inMax) var = inMax;
    return (uint32_t)((var - inMin)) * (outMax - outMin) / (inMax - inMin) + outMin;
}
